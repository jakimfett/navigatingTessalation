Successfully Navigating TessaLation

# @jakimfett's quirks and context

Written from the [unceded lands](https://landback.org/)  
of the Sovereign Santiam peoples,  
left coast of Turtle Island.

Read the [full TessaLation abstract](spec_sheet.md) to get started.

# Values

Generational thinking.  
Intersectional ethics.  
Efficient engineering.

Cultivating systemic antifragility and first principles thinking.

Communication, co-learning, emancipation.

Focus on seeking paths towards future excellence  
(rather than endlessly rehashing.)

# Scheduling
When left to my own devices,  
I schedule tasks in a trinary sequence of  
	(0) plan-prepare  
	(1) lift-execute  
	(2) debrief-document  
		(repeat)

This can look like an entire day (or longer) of each,  
or a single cycle (45 minutes) of fifteen minutes each  
(or anywhere in between).  

I schedule for sustainability of my mental energy,  
layering physical / kinesthetic tasks around periods of problem-solving.  
So, a lift day for catching up on my email will intersperse writing-response tasks with kinesthetic outpouring of energy,  
like jogging, stretching, swimming, doing the dishes, or wood carving.


# Acknowledgements

Standing upon the shoulders of titans, here.

The contents of this document have been made possible through team cooperative efforts,  
and I cannot overstate the utility of working in a group of professionals with complementary skillsets,  
as well as the perspective my team-peers bring to any project we’ve tackled together.

## Family


In many of these matters I also directly benefit  
from the experience and advice of my co-parent and partner,  
who studies cognitive functions and the intersectional mental health impacts  
of persistent antipatterns in accessibility/usability experiences.

Their input has deeply informed and improved  
my practices of cultivating professional relationships  
both inside and outside of engineering circles,

and the positive impact of their research excellence  
(and social awareness)  
on my work  

simply cannot  
be overstated.

(Our child continues to bring new awareness of the world to my perception.)  

===  
Peace on your path.

~ T
