Navigating TessaLation

# @jakimfett's list of quirks and context

> "Genius is simply the ability  
>	to sustain unwavering focus on something  
>		to the exclusion  
>			of all unrelated things."  
>	~ Ken Hines (mentor, visionary, friend)


Thank you for taking the time to contemplate this document.

Here you will find information about my foci & capabilities  
some data about my cognitive function stack & processing,  
and some necessary accommodations within the context of my neurodivergent (autism spectrum) atypicalities.

Written from the unceded lands of the Sovereign Santiam peoples,  
left coast of Turtle Island.

## Values

Generational thinking.  
Intersectional ethics.

Cultivating systemic antifragility and first principles thinking.

Communication, co-learning, emancipation.

## Priorities

Shorten feedback loops.  
Failover without downtime.  
Standardize infrastructure.  
Iterate towards excessive reliability.  
Cultivate self-serve automation toolkits.

Relentlessly optimizing everything within my sphere of influence.

## Cognitive Typology

I'm an MBTI:INTJ,  
Enneagram One (wing two),  
Human Design Manifesting Generator,  

pronouns:  
	i/we | she | they/us

(tessalation is a nonbinary system,  
	and resonates with descriptors like 'mind-choir', 'trans', and 'plurality')


and my top five Strengths Finder themes are  
Ideation, Input, Deliberative, Strategic, and Analytical.

## Communication Style & Accessibility

Autism Spectrum gives me a literal-functional perspective on things,  
while the social contexts of words / nonverbals are often invisible to me.

My words are direct,  
though often more blunt  
than I want or intend to be.

> If my misuse of precision or "dictionary / thesarus meaning" renders hurt feeling,  
> please inform me, and I'll lead with an apology,  
> ~~~  
> and I'll want to excavate later, to avoid repeating my mistakes.

My speach processing has a delay,  
which can interfere with my ability to verbalize in some contexts.

Additionally, context switching to audio-verbal processing can have a high cost in technical velocity.  
On weeks with more synchronous work (lots of meetings), technical progression will move less rapidly.

What you say to me is perspective and information,  
to be analysed, contextualized, and empathized with,  
rather than taken personally.

Within social contexts, once is an anomaly,  
twice is an oddity, and thrice is forming a habit.  
Five or more looks like a pattern to me.

(if you think it's an argument,  
	please clarify that meta,  
		it's likely my intent was active listening awkwardly implemented)

Maintaining theory of mind is sometimes an effort for me,  
if my expectations assume you knew something, and you did not, that's probably on me, call it out.

Infodumping can be a thing.  
Please let me know if you need me to skip to the abstract point.

### Conflict Resolution

Listening actively, taking notes.

Give time and space for processing,  
then clarify the meta,  
	re-orient on shared values,  
		check definitions,  
			document agreement,  
				keep things dynamic and brief.


Focus on seeking paths towards future excellence  
(rather than endlessly rehashing.)

## Scheduling & Time

Asynnchronous is best.

For best results,  
please provide an agenda or topical domain(s),  
or perhaps just make it an email.

For best results, work durations should be increments of 45 minutes.

When left to my own devices,  
I schedule tasks in a trinary sequence of  
	(0) plan-prepare  
	(1) lift-execute  
	(2) debrief-document  
		(repeat)

This can look like an entire day (or longer) of each,  
or a single cycle (45 minutes) of fifteen minutes each  
(or anywhere in between).

I schedule for sustainability of my mental energy,  
layering physical / kinesthetic tasks around periods of problem-solving.  
So, a lift day for catching up on my email will intersperse writing-response tasks with kinesthetic outpouring of energy,  
like jogging, stretching, swimming, doing the dishes, or wood carving.


# Acknowledgements

Standing upon the shoulders of titans, here.

The contents of this document have been made possible through team cooperative efforts,  
and I cannot overstate the utility of working in a group of professionals with complementary skillsets,  
as well as the perspective my team-peers bring to any project we’ve tackled together.


In many of these matters I also directly benefit  
from the experience and advice of my co-parent and partner,  
who studies cognitive functions and the intersectional mental health impacts  
of persistent antipatterns in accessibility/usability experiences.

Their input has deeply informed and improved  
my practices of cultivating professional relationships  
both inside and outside of engineering circles,

and the positive impact of their research excellence  
(and social awareness)  
on my work  

simply cannot  
be overstated.

===
Peace on your path.

~ T

--
Tessa L. H. Lovelace  
https://assorted.tech/lovelace/

Is something missing? Ask me!

# Appendix: Axioms
> "Tell me I'm ignorant, I'll take it in stride...though I'll likely ask you to remediate my lack of understanding."
> ~ Nune

First principles as a first principle.
First duty of the engineer is to emancipate the user.

Do it for multiple reasons.
Small frictions make mountains out of molehills.


# Appendix: Cognitive Typology
Each portion of my cognitive stack has benefits, synergies, and drawbacks,
and I've attempted to format my data for concision & clarity.

Said function stack is expressed,
in terms of Meyers-Briggs/Jungian psychological typology, 
Enneagram of Personality, Human Design, and the GALLUP Strengths Finder report.

Your thoughts are deeply appreciated,
and please let me know you'd like me to expand on any portion of the data I've summarized here!

My study and understanding of these systems has rendered me a healthier and more aware individual,
rendered socially-palatable many of my sharp edges,
and given me tools for effectively expressing my identity and capabilities to others.

If you'd like to discuss my data,
or want to chat about how to start collecting your own,
I'm always delighted to chat about this sort of research - please reach out!.


## Meyers-Briggs Typology Index

My cognitive stack (MBTI:INTJ) is:
 - internal intuition
 - external thinking
 - internal feeling
 - external sensing.

Internal Intuition allows me 
to synthesize vast amounts of data 
and generate probable future outcomes.
This benefits my big-picture thinking and planning capabilities, 
and pairs well with creativity and innovation.
Potential drawbacks include a tendency towards incessant planning, being insatiable for data, and perfectionism.

External Thinking complements my 
rational understanding of external systems, 
and allows me to diagnose, optimize, and find objective solutions.
This renders me efficient and practical, 
able to render order from chaos,
though this can come across as over-intensity or overbearing when I am narrowly focused on an outcome.

Internal Feeling gives me 
strong internally-generated values and guides moral judgements.
This function prompts me to advocate for personal liberty and principles.
I depend on this function for my empathy and authenticity, 
it drives my continual self-improvement, 
and gives me strong internal anchors when under pressure.
This has drawbacks too, as I am sometimes idealistic, 
can come across as dismissive of tradition, 
and can inadvertantly tread upon the unspoken social contracts
this is compounded by the autism-spectrum blindness to interpersonal nonverbals and social assumptions/expectations.

External Sensing is a data-gathering function 
focused on the concrete physical environment, 
and aids in my ability to relexively respond to abrupt changes.
This renders me kinesthetic and new-experience-seeking, 
and allows me to pivot rapidly.
At times, this ability to pivot rapidly 
can come across as impulsive to those unfamiliar with my logical processes.
However, needing to use this function too frequently 
without periods to recharge can lead to sensory overload. 


Examples of historical INTJs are Nikola Tesla, Steven Hawking, and Darwin.

## The Enneagram 
(mine is 1, wing 2) 
juxtaposes fears and motivations, 
and intersects beneficially with several aspects of my MBTI.

My basic motivation is a desire for fairness, accuracy, and order.
This pushes me to be hands-on and make a difference, 
to be aware of the needs of others, 
and to make principled, ethical choices.

My basic fear orients around making incorrect choices.
This is the motivation for voracous introspection, 
striving ceaselessly for objective inspection of motivations & biases.

## Human Design: WIP

My Human Design Arctype is the Manifesting Generator:
https://www.geneticmatrix.com/human-design-manifesting-generator/


## GALLOP Strengths Finder

My top five themes are 
Ideation, Input, Deliberative, Strategic, and Analytical.

As an Ideator, 
innovation happens through concept linking. 
My mind spontaneously reduces mechanisms, 
processes, proposals, ideas, and formulas into basic parts, 
and new solutions arise spontaneously 
from the ways each functional piece inter-relates 
with all the others. 
I ask questions, and I ponder answers.

My Input strength orients around the craving to know more. 
I'm a collector and archiver of knowledge, 
and take joy in expressing ideas with precision. 
I'm hardwired to be as informed as possible before starting new ventures, 
and I think deeply and carefully about my topics of interest.

Deliberative means reflecting on possible outcomes, 
extensive intentional consideration before decisionmaking, 
and highlighting risk to reduce and mitigate unworkable outcomes. 
This theme motivates me to cultivate a deep personal knowledge 
of special and diverse topics, procedures, operations, and activities.

As a Strategic, 
relevant patterns and alternative ways to proceed are my strength. 
I am fascinated by problems that puzzle, confound, or frustrate most people, 
and the process of solutions research and remediation sparks great joy in me. 
In my pursuit of knowledge, I'm constantly seeking the cutting edge of my industry and related areas of interest, 
this deep delving of highly-creative topics is a springboard for group discussion, planning, and anticipation of future events.

My Analytical strength prompts me to search for reasons and causes. 
I'm well-optimized to think about the myriad factors that might affect a sitionation, 
my skepticism grounds me on the realistic and practical. 
This theme gives me insight into identifying most critical tasks, 
execution of methodical action plans, and finding recurring sequences. 
Numbers tell me stories that are hidden for most people.

## Flow State
When focused on an outcome, my awareness of things which are not immediately relevant to my focus will drop away, often for several days.

My sleep cycle becomes "when tired or out of executive functioning",
and interruptions are avoided until reaching a progress marker of some sort.

The benefits of this process are cumulative,
and interface well with asynchronous distributed working,
with seven to ten days being the healthy maximum of this state.

After a flow state cycle,
there is a recharge period that gives diminishing returns for overuse,
this is a tool of infrequent use (1-5 times per year).

While in this mode of processing, the focus is held entirely in my head, my environment becomes a representational kinesthetic simulacrum of problem-solving, and most outside contexts are ignored with prejudice.

# Appendix: Competencies

My technical skillset orients on automation and efficiency,
with a focus on developing domain-specific software accelerators
for the emergent functional needs of my team.

My consulting work has kindled a love for  mentoring remote-first engineering teams,
and I'd like to continue my transition into team facilitation and decision driving
this is what I am focusing much of my self-development time cultivating.

These are some roles which made good use of my strengths.


These projects made strong use of my competencies, which correlates strongly with enjoyment of the work:
 - Definitions and processes for handling protected data (local, cloud, and mobile).
 - Writing a shell scripting library to multithread system management functions.
 - Identifying systemic root causes of platform instability, insecurity, and inefficiency, articulating paths to remediation, and implementing solutions based on organizational priorities and goals.
 - Scaling a vendor from a single manually maintained self-hosted instance of their application to a load balanced infrastructure-as-code with continuous integration and seamless failover between cloud, local, and backup systems.
 - Partnered with another local engineer to build a business plan and path to implementation around a prototype biochemistry process they wanted to get funded.
 - Helping with the coaching/tutoring/onboarding of computer science students/interns as they encounter devops at scale for the first time in a fast-paced organization.
 - Automated testing and reporting for public-facing API endpoints.
 - Immutable infrastructure research and prototype deployments for a hardware-backed microservices cluster.
 - change management process and implementation
 - infrastructure (build pipeline, continuous integration, high availability autoscale dev/sec/ops) architect
 - writing middleware for legacy-to-microservice restructuring of production applications
 - open source software platform conversion & maintenance
 - Centralized logging and automated report generation for database, middleware, and mobile applications.
 - Automated aggregation, pruning/deduplication, encryption, and storage of protected data.
 - Use case mapping, security analysis, and functional need shortfall reports for open-source-based SaaS platforms.
 - Remediation of single-cloud-based downtime vulnerability, to render a client’s critical business applications capable of performing through simultaneous Google, Amazon, and Microsoft total infrastructure outages.


## Deployment Environments 
(languages, applications, and on-premise, cloud, and hybrid platforms)

Apache, Arduino, Backblaze B2, Bourne Again Shell (Bash), BrowserStack, C, C++, CentOS/Red Hat, 
Debian/Raspbian, Docker, Git, Go/Golang, Java, Javascript (Angular, jQuery, Node.js, React), 
Jenkins, Laminar CI, Lua, mySQL/MariaDB, Nginx, NixOS/Guix, PHP (Drupal, Kohana), Pijul, Python, 
Rackspace, Ruby/Rails, Rust, Saltstack, and Vultr, to name a few.

# Historical Roles (or "History in a Nutshell"):
 
## childhood (1990-2006)
    - homeschooled, extensive extracurricular  
    (music, outdoor 'survival', gardening, wood crafting)

    - wanted to be an astronaut, planned on becoming an aerospace engineer

    - navy-programmer-turned-academic parent  
    (inspired me towards tech through their cooperative  
    telecommunications organization participation and tinkering)

    - active role in multiple levels of rural civil infrastructure, agriculture & forestry  
    - apprenticeship in fiberglass manufacturing  
    (safety, processes/defects/outcomes, metalworking)

    - material logistics startup w/ parent & grandparent  
    (non-principle, command line programming & breadboard prototyping, digital logic)

    - computer lab sysadmin  
    (Windows/Linux, lead role, system provisioning & student needs resolution)

    - electronics prototyping startup  
    (sole proprietorship)


## early academic & software (2007-2016)
    - mechanical engineering program  
    (pivot from aerospace to software engineering trajectory,  
    orienting towards natural strengths & capabilities thinking like computers)

    - freeze-dry food manufacturing room operator  
    (seasonal, between academic periods, team lead role)

    - business information systems & administration program  
    (Software dev & Linux focus with emphasis on business systems creation & support)

    - academic program for open source participation, overseas client deployment  
    (first thousand hours on a single project, lead engineer)

    - Desktop support  
    (Microsoft, system images & automation, hardware triage & remediation)

    - freelance software development  
    (open source, remote-first)

    --> PHP  
    (custom full stack, wordpress core & plugins, databases)

    --> cluster computing & Java devops  
    (full stack, fully remote, community builder, principle engineer)

    --> Healthcare  
    (system migrations, versioning, and frontend dev support)

    --> More PHP & dev/sec/ops  
    (Jenkins, software lifecycles, legacy APIs & migration planning, analysis)


## executive (2015-2022)
    - Founding Engineer & CTO  
    (business systems, hardware R&D, physical & digital security,  
    technical hiring + team cultivation processes, negotiation,  
    feedback/satisfaction, many other hats)

    - Consulting Engineer  
    (technical infrastructure outcomes, software/hardware/interpersonal R&D,  
    team facilitation, value mapping, decision driving, etc )

## HyperScale 2021-2022
	- megacorp consultant  
	(digital infrastructure and people hyper-scaling, fintech)


## 2022-2???
	- tbd!


# Appendix: Recommended Reading
- A Liberated Mind / Happiness Trap (Hayes)  
- Antifragile (Taleb)  
- Braiding Sweetgrass (Kimmerer)  
- Conquest of Bread (Kropotkin)  
- One from Many (Hock)  
- Say What You Mean (Sofer)  
- Spin Selling (Rackham)  
- System Error (Sahami, Reich, Weinstein)  
- Turn the Ship Around (Marquet)  
- Truth or Dare (Starhawk)  
- Voice of Knowledge (Ruiz)  
- Wisdom of the Enneagram (Riso, Hudson)  
